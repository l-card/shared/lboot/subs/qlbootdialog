set(QLBOOT_DIALOG_DIR ${CMAKE_CURRENT_LIST_DIR})
set(QLBOOT_DIALOG_INCLUDE_DIRS ${QLBOOT_DIALOG_DIR})

set(QLBOOT_DIALOG_SOURCES 
    ${QLBOOT_DIALOG_DIR}/lboot_proto/lboot_proto.h
    ${QLBOOT_DIALOG_DIR}/QLbootError.h
    ${QLBOOT_DIALOG_DIR}/QLbootError.cpp
    ${QLBOOT_DIALOG_DIR}/QLbootOutRecord.h
    ${QLBOOT_DIALOG_DIR}/QLbootOutRecord.cpp
    ${QLBOOT_DIALOG_DIR}/QLbootDialog.cpp
    ${QLBOOT_DIALOG_DIR}/QLbootDialog.h
    ${QLBOOT_DIALOG_DIR}/QLbootDialog.ui
    )
        

include(${QLBOOT_DIALOG_DIR}/translations/cmake/qtranslation.cmake)
set(QLBOOT_DIALOG_TS_BASENAME qlbootdialog)
qtranslation_generate(QLBOOT_DIALOG_GENFILES ${QLBOOT_DIALOG_DIR}/translations ${QLBOOT_DIALOG_TS_BASENAME} ${QLBOOT_DIALOG_SOURCES})

set(QLBOOT_DIALOG_FILES ${QLBOOT_DIALOG_SOURCES} ${QLBOOT_DIALOG_GENFILES})
