#ifndef LBOOTDIALOG_H
#define LBOOTDIALOG_H

#include <QDialog>
#include <QStringList>
#include <QProcess>
#include <QSharedPointer>
#include <memory>
#include "LQError.h"

namespace Ui {
class QLbootDialog;
}
class QLbootOutRecord;

class QLbootDialog : public QDialog {
    Q_OBJECT

public:
    explicit QLbootDialog(QWidget *parent, const QStringList &args, unsigned finishWaitTime = 0,
                          const QString &lbootExec = QStringLiteral("lboot"));
    ~QLbootDialog() override;
    const LQError &error() const {return m_err;}
    bool isTerminated() const {return m_terminated;}

    void setTerminateWarnMsg(const QString &msg);
    void setRestartOnErrorMaxCnt(int cnt) {
        m_restart_on_err_max = cnt;
    }

    void startBoot();
public Q_SLOTS:
    int exec() override;
Q_SIGNALS:
    void successUpdateFinished();
    void progressInfoUpdated(QSharedPointer<QLbootOutRecord> rec, int total_progress);
private Q_SLOTS:

    void onProcFinished(int exitCode, QProcess::ExitStatus);
    void onProcDataReady();
    void onError(QProcess::ProcessError);
    void showDetail();
    void reject() override;
private:
    void lbootProcStart();
    void lbootProcTerminate();
    void lbootClearProccess(bool terminate = false);

    void setOutTextVisible(bool visible);

    static const int firm_write_start_perc {5};
    static const int firm_write_end_perc   {75};
    static const int firm_rec_start_perc   {80};
    static const int firm_rec_finish_perc  {85};
    static const int firm_start_perc       {90};

    const std::unique_ptr<Ui::QLbootDialog> m_ui;
    QProcess *m_lbootProcess {nullptr};
    bool m_started {false};
    bool m_terminated {false};
    QStringList m_args;
    unsigned m_finishWaitTime;
    int m_restart_on_err_cur {0};
    int m_restart_on_err_max {0};
    LQError m_err;
    QString m_terminate_warn_msg;
    QString m_lboot_exec;
};

#endif // LBOOTDIALOG_H
