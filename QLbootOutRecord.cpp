#include "QLbootOutRecord.h"
#include <QStringList>


QLbootOutRecord::QLbootOutRecord(const QString &line) {
    const QStringList strList {line.split(QChar{':'}, QString::SkipEmptyParts)};
    if (strList.size() >= 2) {
        m_stage = strList[0].trimmed();
        m_status = strList[1].trimmed();
        for (int i=2; i < strList.size(); i++) {
            const QStringList paramStr = strList[i].split(QChar{'='});
            if (paramStr.size() >= 2) {
                m_params[paramStr[0].trimmed()] = paramStr[1].trimmed();
            }
        }
    }
}

QString QLbootOutRecord::stageName() const {
    if (m_stage == stageAppToBoot())
        return tr("Switch from application to bootloader");
    if (m_stage == stageCheckParams())
        return tr("Check input parameters");
    if (m_stage == stagePreopenWait())
        return tr("Waiting boot device");
    if (m_stage == stageOpenDevice())
        return tr("Open device");
    if (m_stage == stageCheckInfo())
        return tr("Check device information");
    if (m_stage == stageGetDevMode())
        return tr("Get device mode");
    if (m_stage == stageCheckDevMode())
        return tr("Check device mode");
    if (m_stage == stageGetBootVersion())
        return tr("Get bootloader version");
    if (m_stage == stageGetDevFlags())
        return tr("Get device state flags");
    if (m_stage == stageGetBootFeatures())
        return tr("Get bootloader features");
    if (m_stage == stageCheckBootVer())
        return tr("Check bootloader version");
    if (m_stage == stageWriteProtData())
        return tr("Write protected data");
    if (m_stage == stageWriteFirmware())
        return tr("Write firmware");
    if (m_stage == stageWriteSign())
        return tr("Write signature/hash");
    if (m_stage == stageRecovery())
        return tr("Recovery main firmware");
    if (m_stage == stageStartApp())
        return tr("Start application");
    return QString{};
}

int QLbootOutRecord::errorCode() const {
    return (status() == statusError()) ? param(paramErrCode()).toInt() : 0;
}

QString QLbootOutRecord::errorMsg() const {
    return param(paramErrString());
}

QString QLbootOutRecord::errorTypeDescr() const {
    const QString type {param(paramErrType())};
    if (type == errorTypeUtility())
        return tr("Utility error");
    if (type == errorTypeDevice())
        return tr("Device error");
    if (type == errorTypeIface())
        return tr("Interface error");
    return QString{};
}
