#ifndef QLBOOTERROR_H
#define QLBOOTERROR_H

#include "LQError.h"
#include <QObject>

class QLbootOutRecord;

class QLbootError : public QObject {
    Q_OBJECT
public:
    static LQError error(const QLbootOutRecord &errRecord);

    static LQError lbootExec();
private:

    static QString errorStr(int errCode);

    static LQError error(int code, const QString &msg);
};

#endif // QLBOOTERROR_H
