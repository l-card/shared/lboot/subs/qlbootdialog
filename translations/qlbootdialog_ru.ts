<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>LbootDialog</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Ошибка</translation>
    </message>
    <message>
        <source>Cannot execute lboot process</source>
        <translation type="vanished">Не удалось запустить утилиту обновления lboot</translation>
    </message>
    <message>
        <source>Hide details...</source>
        <translation type="vanished">Скрыть детали...</translation>
    </message>
    <message>
        <source>Show details...</source>
        <translation type="vanished">Показать детали...</translation>
    </message>
    <message>
        <source>Firmware update</source>
        <translation type="vanished">Обновление прошивки</translation>
    </message>
    <message>
        <source>launch update process</source>
        <translation type="vanished">Запуск процесса обновления</translation>
    </message>
</context>
<context>
    <name>LbootOutRecord</name>
    <message>
        <source>Done successfully</source>
        <translation type="vanished">Выполнено успешно</translation>
    </message>
    <message>
        <source>Check input parameters</source>
        <translation type="vanished">Проверка входных параметров</translation>
    </message>
    <message>
        <source>Open device</source>
        <translation type="vanished">Открытие устройства</translation>
    </message>
    <message>
        <source>Check device information</source>
        <translation type="vanished">Проверка информации об устройстве</translation>
    </message>
    <message>
        <source>Get device mode</source>
        <translation type="vanished">Получение режима работы устройства</translation>
    </message>
    <message>
        <source>Check device mode</source>
        <translation type="vanished">Проверка режима работы устройства</translation>
    </message>
    <message>
        <source>Get bootloader version</source>
        <translation type="vanished">Получение версии загрузчика</translation>
    </message>
    <message>
        <source>Get device state flags</source>
        <translation type="vanished">Получение флагов состояния устройства</translation>
    </message>
    <message>
        <source>Get bootloader features</source>
        <translation type="vanished">Получение информации о возможностях загрузчика</translation>
    </message>
    <message>
        <source>Check bootloader version</source>
        <translation type="vanished">Проверка версии загрузчика</translation>
    </message>
    <message>
        <source>Write protected data</source>
        <translation type="vanished">Запись защищенных данных</translation>
    </message>
    <message>
        <source>Write firmware</source>
        <translation type="vanished">Запись прошивки</translation>
    </message>
    <message>
        <source>Write signature/hash</source>
        <translation type="vanished">Запись подписи/хеш суммы</translation>
    </message>
    <message>
        <source>Recovery main firmware</source>
        <translation type="vanished">Восстановление основной прошивки</translation>
    </message>
    <message>
        <source>Start application</source>
        <translation type="vanished">Запуск рабочей программы</translation>
    </message>
    <message>
        <source>Invalid input parameter</source>
        <translation type="vanished">Ошибка входных параметров</translation>
    </message>
    <message>
        <source>Insufficient input arguments</source>
        <translation type="vanished">Недостаточное количество аргументов</translation>
    </message>
    <message>
        <source>Specified invalid interface</source>
        <translation type="vanished">Указан неверный интерфейс </translation>
    </message>
    <message>
        <source>Cannot open firmware file</source>
        <translation type="vanished">Не удалось открыть файл с прошивкой</translation>
    </message>
    <message>
        <source>Cannot open signature file</source>
        <translation type="vanished">Не удалось открыть файл с подписью</translation>
    </message>
    <message>
        <source>Cannot open hash file</source>
        <translation type="vanished">Не удалось открыть файл с хеш-функцией</translation>
    </message>
    <message>
        <source>Memory allocation error</source>
        <translation type="vanished">Ошибка выделения памяти</translation>
    </message>
    <message>
        <source>Resource allocation error</source>
        <translation type="vanished">Ошибка выделения ресурсов</translation>
    </message>
    <message>
        <source>Bootloader device not found</source>
        <translation type="vanished">Не удалось найти загрузочное устройство</translation>
    </message>
    <message>
        <source>IO request failed</source>
        <translation type="vanished">Ошибка запроса ввода-вывода</translation>
    </message>
    <message>
        <source>Invalid device name</source>
        <translation type="vanished">Неверное название устройства</translation>
    </message>
    <message>
        <source>Invalid serial number</source>
        <translation type="vanished">Неверный серийный номер устройства</translation>
    </message>
    <message>
        <source>Device not in bootloader mode</source>
        <translation type="vanished">Устройство находится не в режиме загрузчика</translation>
    </message>
    <message>
        <source>Bootloader doesn&apos;t support burn withot start application</source>
        <translation type="vanished">Загрузчик не поддерживает запись без запуска прошивки</translation>
    </message>
    <message>
        <source>Bootloader doesn&apos;t support recovery operation</source>
        <translation type="vanished">Загрузчик не поддерживает операцию восстановления основной прошивки</translation>
    </message>
    <message>
        <source>Invalid output format specified</source>
        <translation type="vanished">Указан неверный формат вывода</translation>
    </message>
    <message>
        <source>Bootloader doesn&apos;t support protection data</source>
        <translation type="vanished">Загрузчик не поддерживает запись в защищенную область</translation>
    </message>
    <message>
        <source>Cannot open file with protected data</source>
        <translation type="vanished">Не удалось открыть файл с данными для записи в защищенную область</translation>
    </message>
    <message>
        <source>Invalid rs baudrate specified</source>
        <translation type="vanished">Неверно задана скорость RS-интерфейса</translation>
    </message>
    <message>
        <source>Invalid rs parity specified</source>
        <translation type="vanished">Неверно задана четность RS-интерфейса</translation>
    </message>
    <message>
        <source>Invalid modbus address specified</source>
        <translation type="vanished">Неверно задан Modbus адрес устройства</translation>
    </message>
    <message>
        <source>Cannot create modbus context</source>
        <translation type="vanished">Ну удалось создать контекст Modbus</translation>
    </message>
    <message>
        <source>Cannot set modbus address</source>
        <translation type="vanished">Ну удалось установить Modbus адрес устройства</translation>
    </message>
    <message>
        <source>Cannot open modbus port</source>
        <translation type="vanished">Ну удалось открыть порт Modbus устройства</translation>
    </message>
    <message>
        <source>Cannot read modbus registers</source>
        <translation type="vanished">Не удалось прочитать регистры Modbus</translation>
    </message>
    <message>
        <source>Cannot write modbus registers</source>
        <translation type="vanished">Не удалось записать регистры Modbus</translation>
    </message>
    <message>
        <source>Cannot create socket for TFTP operations</source>
        <translation type="vanished">Ну удалось создать сокет для обмена по TFTP</translation>
    </message>
    <message>
        <source>Send TFTP packet error</source>
        <translation type="vanished">Ошибка передачи TFTP-пакета</translation>
    </message>
    <message>
        <source>Receive TFTP packet error</source>
        <translation type="vanished">Ошибка приема TFTP-пакета</translation>
    </message>
    <message>
        <source>Invalid format of received TFTP packet</source>
        <translation type="vanished">Неверный формат принятого TFTP-пакета</translation>
    </message>
    <message>
        <source>TFTP packet response timeout</source>
        <translation type="vanished">Превышено время ожидания ответа на TFTP-пакет</translation>
    </message>
    <message>
        <source>Invalid TFTP host address specified</source>
        <translation type="vanished">Указан неверный адрес устройства для обновления по TFTP</translation>
    </message>
    <message>
        <source>Invalid UDP port for TFTP interface specified</source>
        <translation type="vanished">Указан неверный UDP-порт для TFTP-интерфейса</translation>
    </message>
    <message>
        <source>Invalid TFTP block size specified</source>
        <translation type="vanished">Указан неверный размер блока данных для обмена по TFTP</translation>
    </message>
    <message>
        <source>Receive TFTP error packet</source>
        <translation type="vanished">Принят пакет TFTP с сообщением об ошибки</translation>
    </message>
    <message>
        <source>Received TFTP packets overflow</source>
        <translation type="vanished">Переполнение принятых данных по TFTP</translation>
    </message>
    <message>
        <source>Receive insufficient data over TFTP</source>
        <translation type="vanished">Принято недостаточно данных по TFTP</translation>
    </message>
    <message>
        <source>Received not expected TFTP packet</source>
        <translation type="vanished">Принятый по TFTP пакет не соответствует ожидаемому</translation>
    </message>
    <message>
        <source>Socket system initialization error</source>
        <translation type="vanished">Ошибка инициализации подсистемы сокетов</translation>
    </message>
    <message>
        <source>Signature check failed</source>
        <translation type="vanished">Ошибка проверки подписи</translation>
    </message>
    <message>
        <source>Cannot write unstable firmware as reserve copy</source>
        <translation type="vanished">Нельзя записать нестабильную прошивку в качестве резервной копии</translation>
    </message>
    <message>
        <source>Firmware file is invalid or for another device type</source>
        <oldsource>Firmware file is for invalid device type</oldsource>
        <translation type="vanished">Файл прошивки неверен или для другого типа устройств</translation>
    </message>
    <message>
        <source>Incorrect sign for start write firmware</source>
        <translation type="vanished">Неверный признак начала записи прошивки</translation>
    </message>
    <message>
        <source>Write this type of firmware is disabled</source>
        <translation type="vanished">Запись данного типа прошивки запрщен</translation>
    </message>
    <message>
        <source>Flash memory write error</source>
        <translation type="vanished">Ошибка записи во флеш-память</translation>
    </message>
    <message>
        <source>Invalid start address in firmware</source>
        <translation type="vanished">Неверный начальный адрес в записываемой прошивке</translation>
    </message>
    <message>
        <source>Invalid request data length</source>
        <translation type="vanished">Неверный размер данных в запросе</translation>
    </message>
    <message>
        <source>Invalid command</source>
        <translation type="vanished">Неверная команда</translation>
    </message>
    <message>
        <source>Unpack operation is not supported</source>
        <translation type="vanished">Операция распаковки не поддерживается</translation>
    </message>
    <message>
        <source>Unpack operation is not complete</source>
        <translation type="vanished">Операция распаковки не была завершена</translation>
    </message>
    <message>
        <source>Firmware integrity check error</source>
        <translation type="vanished">Ошибка проверки целостности записанной прошивки</translation>
    </message>
    <message>
        <source>Invalid flash write sector</source>
        <translation type="vanished">Неверный сектор для записи во флеш-память</translation>
    </message>
    <message>
        <source>Write firmware without signature is disabled</source>
        <translation type="vanished">Запрещена запись прошивки без подписи</translation>
    </message>
    <message>
        <source>Invalid firmware hash</source>
        <translation type="vanished">Передано неверное значение хеш-функции</translation>
    </message>
    <message>
        <source>Write development firmware is disabled</source>
        <translation type="vanished">Запрещена запись прошивки для разработчиков</translation>
    </message>
    <message>
        <source>Unsupported special command code</source>
        <translation type="vanished">Неподдерживаемый код специальной команды</translation>
    </message>
    <message>
        <source>Invalid special command parameters</source>
        <translation type="vanished">Неверное значение специальной команды</translation>
    </message>
    <message>
        <source>Invalid special command sign</source>
        <translation type="vanished">Неверный признак специальной команды</translation>
    </message>
    <message>
        <source>Reserve copy is&apos;t present</source>
        <translation type="vanished">Нет действительной резервной копии</translation>
    </message>
    <message>
        <source>Main application is&apos;t present</source>
        <translation type="vanished">Нет действительной основной прошивки</translation>
    </message>
    <message>
        <source>Invalid firmware size</source>
        <translation type="vanished">Неверный размер файла прошивки</translation>
    </message>
    <message>
        <source>invalid protection block size</source>
        <translation type="vanished">Неверный размер данных для записи в защищенную область</translation>
    </message>
    <message>
        <source>Write protected block is disabled</source>
        <translation type="vanished">Запись защищенных данных запрещена</translation>
    </message>
    <message>
        <source>Protected block integrity check error</source>
        <translation type="vanished">Ошибка проверки целостности записанных защищенных данных</translation>
    </message>
    <message>
        <source>Invalid register address</source>
        <translation type="vanished">Неверный адрес регистров</translation>
    </message>
    <message>
        <source>Operation aborted by another request</source>
        <translation type="vanished">Операция прервана другим запросом</translation>
    </message>
    <message>
        <source>Invalid xz archive format</source>
        <translation type="vanished">Неверный формат xz-архива</translation>
    </message>
    <message>
        <source>Invalid signature format</source>
        <translation type="vanished">Неверный формат подписи</translation>
    </message>
    <message>
        <source>Flash compare operation failed</source>
        <translation type="vanished">Ошибка сравнения записанных во флеш-память данных</translation>
    </message>
    <message>
        <source>Flash erase error</source>
        <translation type="vanished">Ошибка стирания флеш-памяти</translation>
    </message>
    <message>
        <source>Current firmware is not selected</source>
        <translation type="vanished">Не выбрана текущая прошивка</translation>
    </message>
    <message>
        <source>Unsupported firmware type</source>
        <translation type="vanished">Неподдерживаемый тип прошивки</translation>
    </message>
    <message>
        <source>Received not complete signature packet</source>
        <translation type="vanished">Принят не завершенный пакет с подписью</translation>
    </message>
    <message>
        <source>Invalid signature packet format</source>
        <translation type="vanished">Неверный формат подписи</translation>
    </message>
    <message>
        <source>Unsupported signature cryptographic algorithm</source>
        <translation type="vanished">Неподдерживаемый алгоритм подписи</translation>
    </message>
    <message>
        <source>Unsupported signature hash algorithm</source>
        <translation type="vanished">Неподдерживаемый алогритм хеш-функции для подписи</translation>
    </message>
    <message>
        <source>Utility error</source>
        <translation type="vanished">Ошибка приложения</translation>
    </message>
    <message>
        <source>Device error</source>
        <translation type="vanished">Ошибка устройства</translation>
    </message>
    <message>
        <source>Interface error</source>
        <translation type="vanished">Ошибка интерфейса</translation>
    </message>
</context>
<context>
    <name>QLbootDialog</name>
    <message>
        <location filename="../QLbootDialog.ui" line="20"/>
        <source>Firmware update</source>
        <translation>Обновление прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootDialog.ui" line="31"/>
        <source>launch update process</source>
        <translation>Запуск процесса обновления</translation>
    </message>
    <message>
        <location filename="../QLbootDialog.cpp" line="122"/>
        <location filename="../QLbootDialog.cpp" line="153"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Cannot execute lboot process</source>
        <translation type="vanished">Не удалось запустить утилиту обновления lboot</translation>
    </message>
    <message>
        <location filename="../QLbootDialog.cpp" line="163"/>
        <source>Hide details...</source>
        <translation>Скрыть детали...</translation>
    </message>
    <message>
        <location filename="../QLbootDialog.cpp" line="163"/>
        <source>Show details...</source>
        <translation>Показать детали...</translation>
    </message>
    <message>
        <location filename="../QLbootDialog.cpp" line="182"/>
        <source>Firmware update temination</source>
        <translation>Принудительное завершение обновления прошивки</translation>
    </message>
</context>
<context>
    <name>QLbootError</name>
    <message>
        <location filename="../QLbootError.cpp" line="119"/>
        <source>Cannot execute lboot process</source>
        <translation>Не удалось запустить утилиту обновления lboot</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="125"/>
        <source>Invalid input parameter</source>
        <translation>Ошибка входных параметров</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="127"/>
        <source>Insufficient input arguments</source>
        <translation>Недостаточное количество аргументов</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="129"/>
        <source>Specified invalid interface</source>
        <translation>Указан неверный интерфейс</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="131"/>
        <source>Cannot open firmware file</source>
        <translation>Не удалось открыть файл с прошивкой</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="133"/>
        <source>Cannot open signature file</source>
        <translation>Не удалось открыть файл с подписью</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="135"/>
        <source>Cannot open hash file</source>
        <translation>Не удалось открыть файл с хеш-функцией</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="137"/>
        <source>Memory allocation error</source>
        <translation>Ошибка выделения памяти</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="139"/>
        <source>Resource allocation error</source>
        <translation>Ошибка выделения ресурсов</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="141"/>
        <source>Bootloader device not found</source>
        <translation>Не удалось найти загрузочное устройство</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="143"/>
        <source>IO request failed</source>
        <translation>Ошибка запроса ввода-вывода</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="145"/>
        <source>Invalid device name</source>
        <translation>Неверное название устройства</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="147"/>
        <source>Invalid serial number</source>
        <translation>Неверный серийный номер устройства</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="149"/>
        <source>Device not in bootloader mode</source>
        <translation>Устройство находится не в режиме загрузчика</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="151"/>
        <source>Bootloader doesn&apos;t support burn withot start application</source>
        <translation>Загрузчик не поддерживает запись без запуска прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="153"/>
        <source>Bootloader doesn&apos;t support recovery operation</source>
        <translation>Загрузчик не поддерживает операцию восстановления основной прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="155"/>
        <source>Invalid output format specified</source>
        <translation>Указан неверный формат вывода</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="157"/>
        <source>Bootloader doesn&apos;t support protection data</source>
        <translation>Загрузчик не поддерживает запись в защищенную область</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="159"/>
        <source>Cannot open file with protected data</source>
        <translation>Не удалось открыть файл с данными для записи в защищенную область</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="161"/>
        <source>Invalid application to bootloader switch interface specified</source>
        <translation>Указан неверный интерфейс перехода из приложения в режим загрузки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="163"/>
        <source>Application to bootloader switch interface is not specified</source>
        <translation>Интерфейс перехода из приложения в режим загрузки не указан</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="166"/>
        <source>Invalid rs baudrate specified</source>
        <translation>Неверно задана скорость RS-интерфейса</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="168"/>
        <source>Invalid rs parity specified</source>
        <translation>Неверно задана четность RS-интерфейса</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="170"/>
        <source>Invalid modbus address specified</source>
        <translation>Неверно задан Modbus адрес устройства</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="172"/>
        <source>Cannot create modbus context</source>
        <translation>Не удалось создать контекст Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="174"/>
        <source>Cannot set modbus address</source>
        <translation>Не удалось установить адрес устройства для подключения по Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="176"/>
        <source>Cannot open modbus port</source>
        <translation>Не удалось открыть порт для подключения к устройству по Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="178"/>
        <source>Cannot read modbus registers</source>
        <translation>Не удалось прочитать регистры Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="180"/>
        <source>Cannot write modbus registers</source>
        <translation>Не удалось записать регистры Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="183"/>
        <source>Cannot create socket for TFTP operations</source>
        <translation>Не удалось создать сокет для обмена по TFTP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="185"/>
        <source>Send TFTP packet error</source>
        <translation>Ошибка передачи TFTP-пакета</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="187"/>
        <source>Receive TFTP packet error</source>
        <translation>Ошибка приема TFTP-пакета</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="189"/>
        <source>Invalid format of received TFTP packet</source>
        <translation>Неверный формат принятого TFTP-пакета</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="191"/>
        <source>TFTP packet response timeout</source>
        <translation>Превышено время ожидания ответа на TFTP-пакет</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="193"/>
        <source>Invalid TFTP host address specified</source>
        <translation>Указан неверный адрес устройства для обновления по TFTP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="195"/>
        <source>Invalid UDP port for TFTP interface specified</source>
        <translation>Указан неверный UDP-порт для TFTP-интерфейса</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="197"/>
        <source>Invalid TFTP block size specified</source>
        <translation>Указан неверный размер блока данных для обмена по TFTP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="199"/>
        <source>Receive TFTP error packet</source>
        <translation>Принят пакет TFTP с сообщением об ошибки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="201"/>
        <source>Received TFTP packets overflow</source>
        <translation>Переполнение принятых данных по TFTP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="203"/>
        <source>Receive insufficient data over TFTP</source>
        <translation>Принято недостаточно данных по TFTP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="205"/>
        <source>Received not expected TFTP packet</source>
        <translation>Принятый по TFTP пакет не соответствует ожидаемому</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="207"/>
        <location filename="../QLbootError.cpp" line="223"/>
        <source>Socket system initialization error</source>
        <translation>Ошибка инициализации подсистемы сокетов</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="209"/>
        <source>Connection was reset by another side (port unreachable?)</source>
        <translation>Соединение сброшено другой стороной (порт недостижим?)</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="212"/>
        <source>HTTP Server response error</source>
        <translation>Сервер ответил сообщением об ошибке на HTTP-запрос</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="214"/>
        <source>Receive insufficient data size</source>
        <translation>Принято недостаточно данных</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="216"/>
        <source>HTTP transfer timeout</source>
        <translation>Превышено время ожидания завершения обмена по HTTP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="219"/>
        <source>Invalid IP-address for UDP connection specified</source>
        <translation>Неверно указан IP-адрес для подключения по UDP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="221"/>
        <source>Invalid UDP port specifeid</source>
        <translation>Указан неверный UDP-порт</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="225"/>
        <source>UDP packet send error</source>
        <translation>Ошибка передачи UDP-пакета</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="227"/>
        <source>UDP message is not specified</source>
        <translation>Не было указано сообщение, передаваемое по UDP</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="230"/>
        <source>Device IP-address is not specified</source>
        <translation>Не указан IP-адрес устройства</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="232"/>
        <source>Modbus registers start address is not specified</source>
        <translation>Не укзаан начальный адрес регистров Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="234"/>
        <source>Modbus registers write values are not specified</source>
        <translation>Не указаны значения для записи в регистры Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="236"/>
        <source>Invalid Modbus TCP port specified</source>
        <translation>Указан неверный номер TCP-порта для подключения по Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="238"/>
        <source>Invalid Modbus registers start address specified</source>
        <translation>Указан неверный стартовый адрес регистров Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="240"/>
        <source>Invalid modbus register values format</source>
        <translation>Неверный формат значений для записи в регистры Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="242"/>
        <source>Invalid modbus register values count</source>
        <translation>Неверное значение количества регистров Modbus при обмене</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="244"/>
        <source>Modbus context creation error</source>
        <translation>Ошибка создания контекста Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="246"/>
        <source>Modbus connection error</source>
        <translation>Ошибка подключения к устройству по Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="248"/>
        <source>Modbus registers write error</source>
        <translation>Ошибка записи в регистры Modbus</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="251"/>
        <source>Signature check failed</source>
        <translation>Ошибка проверки подписи</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="253"/>
        <source>Cannot write unstable firmware as reserve copy</source>
        <translation>Нельзя записать нестабильную прошивку в качестве резервной копии</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="255"/>
        <source>Firmware file is invalid or for another device type</source>
        <translation>Файл прошивки неверен или для другого типа устройств</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="257"/>
        <source>Incorrect sign for start write firmware</source>
        <translation>Неверный признак начала записи прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="259"/>
        <source>Write this type of firmware is disabled</source>
        <translation>Запись данного типа прошивки запрщен</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="261"/>
        <source>Flash memory write error</source>
        <translation>Ошибка записи во флеш-память</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="263"/>
        <source>Invalid start address in firmware</source>
        <translation>Неверный начальный адрес программы в записываемой прошивке</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="265"/>
        <source>Invalid request data length</source>
        <translation>Неверный размер данных в запросе</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="267"/>
        <source>Invalid command</source>
        <translation>Неверная команда</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="269"/>
        <source>Unpack operation is not supported</source>
        <translation>Операция распаковки не поддерживается</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="271"/>
        <source>Unpack operation is not complete</source>
        <translation>Операция распаковки не была завершена</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="273"/>
        <source>Firmware integrity check error</source>
        <translation>Ошибка проверки целостности записанной прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="275"/>
        <source>Invalid flash write sector</source>
        <translation>Неверный сектор для записи во флеш-память</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="277"/>
        <source>Write firmware without signature is disabled</source>
        <translation>Запрещена запись прошивки без подписи</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="279"/>
        <source>Invalid firmware hash</source>
        <translation>Передано неверное значение хеш-функции</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="281"/>
        <source>Write development firmware is disabled</source>
        <translation>Запрещена запись прошивки для разработчиков</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="283"/>
        <source>Unsupported special command code</source>
        <translation>Неподдерживаемый код специальной команды</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="285"/>
        <source>Invalid special command parameters</source>
        <translation>Неверное значение специальной команды</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="287"/>
        <source>Invalid special command sign</source>
        <translation>Неверный признак специальной команды</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="289"/>
        <source>Reserve copy is&apos;t present</source>
        <translation>Нет действительной резервной копии</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="291"/>
        <source>Main application is&apos;t present</source>
        <translation>Нет действительной основной прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="293"/>
        <source>Invalid firmware size</source>
        <translation>Неверный размер файла прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="295"/>
        <source>invalid protection block size</source>
        <translation>Неверный размер данных для записи в защищенную область</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="297"/>
        <source>Write protected block is disabled</source>
        <translation>Запись защищенных данных запрещена</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="299"/>
        <source>Protected block integrity check error</source>
        <translation>Ошибка проверки целостности записанных защищенных данных</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="301"/>
        <source>Invalid register address</source>
        <translation>Неверный адрес регистров</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="303"/>
        <source>Operation aborted by another request</source>
        <translation>Операция прервана другим запросом</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="305"/>
        <source>Invalid xz archive format</source>
        <translation>Неверный формат xz-архива</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="307"/>
        <source>Invalid signature format</source>
        <translation>Неверный формат подписи</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="309"/>
        <source>Flash compare operation failed</source>
        <translation>Ошибка сравнения записанных во флеш-память данных</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="311"/>
        <source>Flash erase error</source>
        <translation>Ошибка стирания флеш-памяти</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="313"/>
        <source>Current firmware is not selected</source>
        <translation>Не выбрана текущая прошивка</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="315"/>
        <source>Unsupported firmware type</source>
        <translation>Неподдерживаемый тип прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="317"/>
        <source>Flash write enable error</source>
        <translation>Ошибка разрешения записи во флеш-память</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="319"/>
        <source>Flash write disable error</source>
        <translation>Ошибка запрета записи во флеш-память</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="321"/>
        <source>Flash read protection code write error</source>
        <translation>Ошибка установки уровня защиты от чтения флеш-памяти микроконтроллера</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="323"/>
        <source>Invalid firmware file format</source>
        <translation>Некорректный формат файла прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="325"/>
        <source>No firmware hash present in file</source>
        <translation>Файл не содержит хеш блоков прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="327"/>
        <source>File contains unsupported firmware block count</source>
        <translation>Файл содержит неподдерживаемое количество блоков данных</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="329"/>
        <source>Invalid firmware region number</source>
        <translation>Неверный номер региона прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="331"/>
        <source>Main region data was not found in file</source>
        <translation>Не обнаружены данные для главного региона в прошивке</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="333"/>
        <source>File is not allowed to be used as specified firmware type</source>
        <translation>Данный файл не разрешено использовать в качестве указанного типа прошивки</translation>
    </message>
    <message>
        <source>File is not allowed to beused as specified firmware type</source>
        <translation type="vanished">Данный файл не разрешено использовать в качестве указанного типа прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="335"/>
        <source>Firmware is for invalid chip type</source>
        <translation>Прошивка не предназначена для используемого типа чипа</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="337"/>
        <source>Signature is not present in file</source>
        <translation>Отсутствует подпись в файле прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="339"/>
        <source>Metafile format is not supported by device</source>
        <translation>Мета-формат не поддерживается устройством</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="341"/>
        <source>Metafile format is required for this device</source>
        <translation>Для данного устройства прошивка должен использоваться мета-формат файла прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="343"/>
        <source>Cannot decrypt data in file</source>
        <translation>Невозможно расшифровать переданные данные</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="345"/>
        <source>Firmware data must be encrypted</source>
        <translation>Данные прошивки должны быть зашифрованы</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="347"/>
        <source>Firmware data is not present in file</source>
        <translation>Не обнаружены данные прошивки в принятом файле</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="349"/>
        <source>Invalid encryption key</source>
        <translation>Использован неверный ключ шифрования прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="351"/>
        <source>Valid file ending is not detected</source>
        <translation>Не обнаружено корректного окончания передаваемого файла</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="354"/>
        <source>Received not complete signature packet</source>
        <translation>Принят не завершенный пакет с подписью</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="356"/>
        <source>Invalid signature packet format</source>
        <translation>Неверный формат подписи</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="358"/>
        <source>Unsupported signature cryptographic algorithm</source>
        <translation>Неподдерживаемый алгоритм подписи</translation>
    </message>
    <message>
        <location filename="../QLbootError.cpp" line="360"/>
        <source>Unsupported signature hash algorithm</source>
        <translation>Неподдерживаемый алогритм хеш-функции для подписи</translation>
    </message>
</context>
<context>
    <name>QLbootOutRecord</name>
    <message>
        <location filename="../QLbootDialog.cpp" line="84"/>
        <source>Done successfully</source>
        <translation>Выполнено успешно</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="21"/>
        <source>Switch from application to bootloader</source>
        <translation>Переключение в режим загрузчика</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="23"/>
        <source>Check input parameters</source>
        <translation>Проверка входных параметров</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="25"/>
        <source>Waiting boot device</source>
        <translation>Ожидание загрузочного устройства</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="27"/>
        <source>Open device</source>
        <translation>Открытие устройства</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="29"/>
        <source>Check device information</source>
        <translation>Проверка информации об устройстве</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="31"/>
        <source>Get device mode</source>
        <translation>Получение режима работы устройства</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="33"/>
        <source>Check device mode</source>
        <translation>Проверка режима работы устройства</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="35"/>
        <source>Get bootloader version</source>
        <translation>Получение версии загрузчика</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="37"/>
        <source>Get device state flags</source>
        <translation>Получение флагов состояния устройства</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="39"/>
        <source>Get bootloader features</source>
        <translation>Получение информации о возможностях загрузчика</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="41"/>
        <source>Check bootloader version</source>
        <translation>Проверка версии загрузчика</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="43"/>
        <source>Write protected data</source>
        <translation>Запись защищенных данных</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="45"/>
        <source>Write firmware</source>
        <translation>Запись прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="47"/>
        <source>Write signature/hash</source>
        <translation>Запись подписи/хеш суммы</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="49"/>
        <source>Recovery main firmware</source>
        <translation>Восстановление основной прошивки</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="51"/>
        <source>Start application</source>
        <translation>Запуск рабочей программы</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="66"/>
        <source>Utility error</source>
        <translation>Ошибка приложения</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="68"/>
        <source>Device error</source>
        <translation>Ошибка устройства</translation>
    </message>
    <message>
        <location filename="../QLbootOutRecord.cpp" line="70"/>
        <source>Interface error</source>
        <translation>Ошибка интерфейса</translation>
    </message>
</context>
</TS>
