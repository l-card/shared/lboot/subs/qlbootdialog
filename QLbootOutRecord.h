#ifndef QLBOOTOUTRECORD_H
#define QLBOOTOUTRECORD_H

#include <QObject>
#include <QString>
#include <QHash>

class QLbootOutRecord : public QObject {
    Q_OBJECT
public:
    explicit QLbootOutRecord(const QString &line);

    /* варианты статусов */
    static QString statusStart()            {return QStringLiteral("start");}
    static QString statusProgress()         {return QStringLiteral("progress");}
    static QString statusDone()             {return QStringLiteral("done");}
    static QString statusError()            {return QStringLiteral("error");}
    static QString statusWarning()          {return QStringLiteral("warning");}

    /* варианты этапов выполнения */
    static QString stageAppToBoot()         {return QStringLiteral("app_to_boot");}
    static QString stageCheckParams()       {return QStringLiteral("check_params");}
    static QString stagePreopenWait()       {return QStringLiteral("preopen_wait");}
    static QString stageOpenDevice()        {return QStringLiteral("open_device");}
    static QString stageCheckInfo()         {return QStringLiteral("check_info");}
    static QString stageGetDevMode()        {return QStringLiteral("get_devmode");}
    static QString stageCheckDevMode()      {return QStringLiteral("check_devmode");}
    static QString stageGetBootVersion()    {return QStringLiteral("get_bootver");}
    static QString stageCheckBootVer()      {return QStringLiteral("check_bootver");}
    static QString stageGetDevFlags()       {return QStringLiteral("get_devflags");}
    static QString stageGetBootFeatures()   {return QStringLiteral("get_features");}
    static QString stageWriteFirmware()     {return QStringLiteral("write_firmware");}
    static QString stageWriteSign()         {return QStringLiteral("write_sign");}
    static QString stageRecovery()          {return QStringLiteral("recovery");}
    static QString stageStartApp()          {return QStringLiteral("start_app");}
    static QString stageWriteProtData()     {return QStringLiteral("wr_prot_data");}


    static QString paramCplSize()           {return QStringLiteral("cpl_size");}
    static QString paramFullSize()          {return QStringLiteral("full_size");}
    static QString paramErrCode()           {return QStringLiteral("err_code");}
    static QString paramErrType()           {return QStringLiteral("err_type");}
    static QString paramErrString()         {return QStringLiteral("err_str");}

    static QString errorTypeUtility()       {return QStringLiteral("utility");}
    static QString errorTypeIface()         {return QStringLiteral("interface");}
    static QString errorTypeDevice()        {return QStringLiteral("device");}

    QString stageName() const;

    bool valid() const {return !m_stage.isEmpty();}
    QString stage() const {return m_stage;}
    QString status() const {return m_status;}
    QString param(const QString &name) const {return m_params[name];}

    QString errorMsg() const;
    QString errorTypeDescr() const;
    int errorCode() const;

private:

    QString m_stage;
    QString m_status;
    QHash<QString, QString> m_params;
};

#endif // QLBOOTOUTRECORD_H
